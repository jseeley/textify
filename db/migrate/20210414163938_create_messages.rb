class CreateMessages < ActiveRecord::Migration[6.1]
  def change
    create_table :phone_numbers do |t|
      t.string  :phone_number, limit: 10
      t.boolean :is_valid, default: true

      t.timestamps
    end
    add_index :phone_numbers, [:phone_number], :unique => true

    create_table :messages do |t|
      t.belongs_to :phone_number, index: true, foreign_key: true
      t.string :message
      t.string :status
      t.string :guid

      t.timestamps
    end
  end
end
