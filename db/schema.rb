# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_04_14_163938) do

  create_table "messages", force: :cascade do |t|
    t.integer "phone_number_id"
    t.string "message"
    t.string "status"
    t.string "guid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["phone_number_id"], name: "index_messages_on_phone_number_id"
  end

  create_table "phone_numbers", force: :cascade do |t|
    t.string "phone_number", limit: 10
    t.boolean "is_valid", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["phone_number"], name: "index_phone_numbers_on_phone_number", unique: true
  end

  add_foreign_key "messages", "phone_numbers"
end
