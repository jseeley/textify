require 'rails_helper'

RSpec.describe LoadBalancingWorker do
  describe 'object initialization' do
    it 'should use index 1 by default' do
      load_balancer = LoadBalancingWorker.new
      expect(load_balancer.index).to eq(1)
    end

    it 'should not have attempted connecting to a provider' do
      load_balancer = LoadBalancingWorker.new
      expect(load_balancer.attempted).to be_falsy
    end
  end

  describe 'negotiating provider' do
    it 'should return provider 2 URI' do
      load_balancer = LoadBalancingWorker.new
      allow(load_balancer).to receive(:flip_providers).and_return(1)
      load_balancer.get_balanced_provider
      expect(load_balancer.get_balanced_provider.path).to eq('/dev/provider2')
    end

    it 'should return a new provider if the other has already been attempted' do
      load_balancer = LoadBalancingWorker.new
      first_provider = load_balancer.get_balanced_provider
      other_provider = load_balancer.get_unattempted_provider
      expect(first_provider.path).not_to eq(other_provider.path)
    end
  end

end
