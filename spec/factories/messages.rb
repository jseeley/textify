FactoryBot.define do
  factory :message do
    message { "MyString" }
    status { "MyString" }
    phone_number { "MyString" }
    references { "MyString" }
    guid { "MyString" }
  end
end
