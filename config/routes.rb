Rails.application.routes.draw do
  post "/messages/create", to: "messages#create"
  get "/messages/show/:id", to: "messages#show"
  post "/messages/callback", to: "messages#callback"

  get "/phone_numbers/show/:id", to: "phone_numbers#show"

  # Mounting a light weight Sinatra application
  # to provide a web page without needing the whole Rails
  # framework since this is primarily just an API.
  mount MyApp.new => '/'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
