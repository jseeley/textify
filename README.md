# README

### Summary

For ease of use I have deployed the project to **heroku** and setup the domain [http://www.textify.fans](http://www.textify.fans) to easily access this project -- _no setup required_!

Documentation on using the API is available on the home page at [http://www.textify.fans](http://www.textify.fans).

### Code

Go ahead and poke around the code here or clone it using `git clone https://gitlab.com/jseeley/textify.git`

I have included a minimal rspec set of tests for the `LoadBalancingWorker` class.