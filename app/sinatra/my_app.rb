require 'sinatra/base'

# Bolted in the world's simplest Sinatra app since Heroku won't
# directly serve static files. This allows me to get around that
# to create my static frontpage and links to documenation.
class MyApp < Sinatra::Base

    get '/' do
      redirect "index.html"
    end

end
