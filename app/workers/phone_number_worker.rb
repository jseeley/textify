# Right now this doesn't do much and could
# have been just in-lined to the controller method,
# but the idea is to separate out some of the
# controller logic which serves to make things more re-usable
# and easier to test.
class PhoneNumberWorker
  # Looks up a phone number to determine if it has been marked as invalid
  #
  # @return [PhoneNumber, bool] returns the phone number if valid, otherwise false
  def self.is_valid?(phone_number)
    number = PhoneNumber.find_or_create_by(phone_number: phone_number)
    number.is_valid ? number : false
  end
end
