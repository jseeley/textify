# Very basic load balancer that should average roughly 30%/70%
# split between two providers using random numbers
#
# This could be pretty easily be updated to a more mature load balancing technique
# without needing to make many changes to outside dependencies.
class LoadBalancingWorker
    @uris = [URI("https://jo3kcwlvke.execute-api.us-west-2.amazonaws.com/dev/provider1"),
            URI("https://jo3kcwlvke.execute-api.us-west-2.amazonaws.com/dev/provider2")]

    attr_reader :index
    attr_accessor :attempted

    def initialize
      @index = 1
      @attempted = false
    end

    # Randomly selects one of the two providers
    #
    # @return [URI] the URI object that will subsequently be used to make Net::HTTP requests
    def get_balanced_provider
      random_roll = rand(0..9)
      if [0,1,2].include? random_roll
        self.flip_providers
      end
      self.class.uri(@index)
    end

    # If one of the providers has failed this will return back the other
    # provider that has not yet been attempted.
    #
    # @return [URI] the URI object that will subsequently be used to make Net::HTTP requests
    def get_unattempted_provider
      self.flip_providers
      self.class.uri(@index)
    end

    # Private method to switch between the two providers
    def flip_providers
      @index = 1 - @index
    end
    private :flip_providers

    class << self
      def uri(index)
        @uris[index]
      end
    end
end
