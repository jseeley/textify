class PhoneNumberSerializer
  include JSONAPI::Serializer

  set_type :phone_number
  set_id :id
  attributes :phone_number, :is_valid
end
