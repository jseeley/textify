class MessageSerializer
  include JSONAPI::Serializer

  set_type :message
  set_id :id
  attributes :message, :status

  belongs_to :phone_number
end
