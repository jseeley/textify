require "uri"
require "net/http"

class MessagesController < ApplicationController
  def create

    phone_number = PhoneNumberWorker.is_valid?(params[:phone_number])
    if phone_number
      load_balancer = LoadBalancingWorker.new
      uri = load_balancer.get_balanced_provider

      begin
        https = Net::HTTP.new(uri.host, uri.port)
        https.use_ssl = true

        request = Net::HTTP::Post.new(uri)
        request["Content-Type"] = "application/json"

        request.body = {
          to_number: params['phone_number'],
          message: params['message'],
          callback_url: "#{self.request.base_url}/messages/callback"
        }
        request.body = request.body.to_json
        response = https.request(request)

        body = JSON.parse(response.read_body)

        if response.code == "200"
          message = Message.create!(guid: body['message_id'], phone_number: phone_number, message: params['message'], status: 'queued')
          render json: MessageSerializer.new(message).serializable_hash.to_json, status: 200
        else
          if response.code == "500"
            raise "A service provider error has occurred."
          else
            raise "An unexpected error has occurred"
          end
        end
      rescue => error
        unless load_balancer.attempted
          uri = load_balancer.get_unattempted_provider
          retry
        else
          Message.create!(phone_number: phone_number, message: params['message'], status: 'failed')
        end
      end
    else
      render json: { errors: "The requested phone number #{params['phone_number']} is invalid"}, status: :bad_request
    end

  end

  def show
    message = Message.find(params['id'])
    render json: MessageSerializer.new(message).serializable_hash.to_json, status: 200
  end

  def callback
    message = Message.find_by(guid: params['message_id'])
    if message.status != 'delivered'
      message.update(status: params['status'])
      message.save
    end

    if params['status'] == 'invalid'
      message.phone_number.update(is_valid: false)
      message.phone_number.save
    end
  end
end
