class PhoneNumbersController < ApplicationController
  def show
    phone_number = PhoneNumber.find(params['id'])
    render json: PhoneNumberSerializer.new(phone_number).serializable_hash.to_json, status: 200
  end
end
